//
//  NetworkManagerStub.swift
//  Recruitment-iOSTests
//
//  Created by Yuriy Paterega on 21.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

@testable import Recruitment_iOS

class NetworkManagerStub: NetworkingManager {
    var downloadItemsCalled = false
    var downloadItemWithIDCalled = false
    var itemsModelResultToBeReturned: Result<ItemsModel,Error> = .success(ItemsModel())
    var itemDetailsModelResultToBeReturned: Result<ItemDetailsModel,Error> = .success(ItemDetailsModel())
    
    override func downloadItems(_ completionHandler: @escaping ((Result<ItemsModel,Error>)-> ())) {
        downloadItemsCalled = true
        completionHandler(itemsModelResultToBeReturned)
    
    }
    
    override func downloadItemWithID(_ id: String, completionHandler: @escaping  ((Result<ItemDetailsModel,Error>)-> ())) {
        downloadItemWithIDCalled = true
        completionHandler(itemDetailsModelResultToBeReturned)
    }
    
}
