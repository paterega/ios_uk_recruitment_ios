//
//  DetailsControllerTest.swift
//  Recruitment-iOSTests
//
//  Created by Yuriy Paterega on 21.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit
import Foundation
import XCTest
@testable import Recruitment_iOS

class DetailsControllerTests: XCTestCase {
    
    let detailsViewSpy = DetailsViewMock()
    let networkManagerStub = NetworkManagerStub()
    var detailsPresenter: DetailsViewPresenter!
    
    override func setUp() {
        super.setUp()
        detailsPresenter = DetailsViewPresenter(networkingManager: networkManagerStub)
        detailsPresenter.view = detailsViewSpy
    }
    
    override func tearDown() {
        detailsPresenter = nil
    }
    
    func test_updateModel() {
        let mockId = "1"
        
        let itemsDetailsModelMock = ItemDetailsModel(data: ItemDetailModel(id: mockId, attributes: ItemDetailsModelAttributes(name: "name", desc: "desc", color: Color(rawValue: "Red"))))
        
        networkManagerStub.itemDetailsModelResultToBeReturned = .success(itemsDetailsModelMock)
        detailsPresenter.getDataForItem(mockId)
        
        XCTAssertEqual(detailsViewSpy.updateViewModelCalled, true)
        XCTAssertTrue(!detailsViewSpy.viewControllerModel.descriptionText.isEmpty)
    }
    
}

class DetailsViewMock: DetailsViewControllerProtocol {
    var updateViewModelCalled = false
    var viewControllerModel: DetailsViewController.ViewModel = DetailsViewController.ViewModel()

    func updateViewModel(with model: DetailsViewController.ViewModel) {
        updateViewModelCalled = true
        viewControllerModel = model
    }

}
