//
//  TableViewControllerTests.swift
//  Recruitment-iOSTests
//
//  Created by Yuriy Paterega on 21.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit
import Foundation
import XCTest
@testable import Recruitment_iOS

class TableViewTests: XCTestCase {
    
    let tableViewSpy = TableViewMock()
    let networkManagerStub = NetworkManagerStub()
    var tableViewPresenter: TableViewPresenter!
    
    override func setUp() {
        super.setUp()
        tableViewPresenter = TableViewPresenter(networkingManager: networkManagerStub)
        tableViewPresenter.view = tableViewSpy
    }
    
    override func tearDown() {
        tableViewPresenter = nil
    }
    
    func test_updateModel() {
        let itemsModelMock = ItemsModel(data: [ItemModel(id: "id", attributes: ItemModelAttributes(name: "name", preview: "preview", color: Color(rawValue: "Green")))])
        
        networkManagerStub.itemsModelResultToBeReturned = .success(itemsModelMock)
        tableViewPresenter.getData()
        
        XCTAssertTrue(tableViewSpy.updateViewModelCalled, "updateViewModelCalled should be called")
        XCTAssertEqual(tableViewSpy.viewControllerModel.itemModels.count, 1 , "itemModels in vc should be equal to 1")
    }
    
    func test_updateModel_failed() {
        networkManagerStub.itemsModelResultToBeReturned = .failure(NetworkingManager.ParseError.parsingError)
        tableViewPresenter.getData()
        
        XCTAssertFalse(tableViewSpy.updateViewModelCalled)
        XCTAssertEqual(tableViewSpy.viewControllerModel.itemModels.count, 0)
    }
    
    func test_selectRow_with_noData() {
        tableViewPresenter.selectedItem(index: 0)
        XCTAssertEqual(tableViewSpy.selectedItemCalled, false)
    }
    
    func test_selectRow() {
        let itemsModelMock = ItemsModel(data: [ItemModel(id: "id", attributes: ItemModelAttributes(name: "name", preview: "preview", color: Color(rawValue: "Green")))])
        networkManagerStub.itemsModelResultToBeReturned = .success(itemsModelMock)
        tableViewPresenter.getData()
        
        XCTAssertTrue(tableViewSpy.updateViewModelCalled)
        XCTAssertEqual(tableViewSpy.viewControllerModel.itemModels.count, 1)
        
        tableViewPresenter.selectedItem(index: 0)
        XCTAssertEqual(tableViewSpy.selectedItemCalled, true)
    }
    
    
}

class TableViewMock: TableViewControllerProtocol {
    
    var updateViewModelCalled = false
    var selectedItemCalled = false
    var itemModel: ItemModel = ItemModel()
    var viewControllerModel: TableViewController.ViewModel = TableViewController.ViewModel()
    
    func updateViewModel(with model: TableViewController.ViewModel) {
        updateViewModelCalled = true
        viewControllerModel = model
    }
    
    func selectedItem(item: ItemModel) {
        selectedItemCalled = true
        itemModel = item
    }
    
}
