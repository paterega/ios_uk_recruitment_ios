//
//  SplashScreenCoordinator.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 18.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

class SplashScreenCoordinator: Coordinator {

    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        showSplashScreen()
    }
}

extension SplashScreenCoordinator {
    private func showSplashScreen() {
        let viewController = SplashScreenViewController()
        navigationController.viewControllers = [viewController]
        
        viewController.onStartButtonPressed = {[weak self] in
            self?.presentTableView()
        }
    }
    
    private func presentTableView() {
        let tabBarController = TabBarController()
        navigationController.isNavigationBarHidden = false
        navigationController.pushViewController(tabBarController, animated: false)
    }
}
