//
//  SplashScreenPresenter.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 18.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

class SplashScreenPresenter {
    weak var view: SplashScreenViewProtocol?
    
    init() {}
    
    func attachView(view: SplashScreenViewProtocol) {
        self.view = view
    }
    
    func dettachView() {
        view = nil
    }
}
