//
//  SplashScreenViewController.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 18.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

private struct Configuration {
    let backgroundColor = UIColor.white
    let buttonText = "start"
    let buttonColor: UIColor = UIColor(red: 0, green: 0.478431, blue: 1, alpha: 1)
    let buttonTextFont: UIFont = UIFont.systemFont(ofSize: 15)
}

protocol SplashScreenViewProtocol: AnyObject {}

class SplashScreenViewController: UIViewController {
    private let config = Configuration()
    @UsesAutoLayout private var startButton = UIButton ()
    var onStartButtonPressed: VoidClosure?
    private let presenter = SplashScreenPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setBackgroundColor()
        setupView()
    }
    
    deinit {
        presenter.dettachView()
    }
}

extension SplashScreenViewController {
    private func setupView() {
        setupButton()
    }
    
    private func setBackgroundColor() {
        view.backgroundColor = config.backgroundColor
    }
    
    private func setupButton() {
        view.addSubview(startButton)
        startButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        startButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        startButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        startButton.heightAnchor.constraint(equalToConstant: 100).isActive = true
        startButton.setAttributedTitle(setAttibutedTitleText(), for: .normal)
        startButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }
    
    @objc func buttonAction(){
        onStartButtonPressed?()
    }
    
    private func setAttibutedTitleText() -> NSAttributedString {
        let string = config.buttonText.uppercased()
        let myAttributes = [NSAttributedString.Key.foregroundColor: config.buttonColor,
                            NSAttributedString.Key.font: config.buttonTextFont]
        return NSAttributedString(string: string, attributes: myAttributes)
    }
}


extension SplashScreenViewController: SplashScreenViewProtocol {}
