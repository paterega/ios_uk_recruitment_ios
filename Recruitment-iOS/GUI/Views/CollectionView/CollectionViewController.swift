//
//  CollectionViewController.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 18.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

private struct Configuration {
    let backgroundColor = UIColor.white
}

protocol CollectionViewControllerProtocol: AnyObject {
    func updateViewModel(with model: CollectionViewController.ViewModel)
    func selectedItem(item: ItemModel)
}

extension CollectionViewController {
    struct ViewModel {
        var itemModels = [CollectionCellConfigurator]()
    }
}

class CollectionViewController: BaseCollectionViewController {
    private let config = Configuration()
    private let presenter = CollectionViewPresenter(networkingManager: NetworkingManager.shared)
    var selectedItem: ItemModelClosure?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        presenter.getData()
        setupCollectionView()
        setupBackgroundColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setEmptyViewControllerTitle()
    }
    
    deinit {
        presenter.dettachView()
    }
    
}

extension CollectionViewController {
    private func setupBackgroundColor() {
        view.backgroundColor = config.backgroundColor
    }
    
    private func setEmptyViewControllerTitle() {
        tabBarController?.navigationItem.title = ""
    }
    
    private func setupCollectionView() {
        collectionView.register(CollectionViewCell.self)
        collectionViewDelegate.selectedCollectionViewCell = { [weak self] index in
            self?.presenter.selectedItem(index: index)
        }
    }
    
}


extension CollectionViewController: CollectionViewControllerProtocol {
    func updateViewModel(with model: CollectionViewController.ViewModel) {
        addCells(model.itemModels)
    }
    
    func selectedItem(item: ItemModel) {
        selectedItem?(item)
    }
    
}
