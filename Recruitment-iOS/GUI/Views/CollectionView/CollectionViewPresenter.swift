//
//  CollectionViewPresenter.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 18.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import Foundation

class CollectionViewPresenter {
    weak var view: CollectionViewControllerProtocol?
    private let networkingManager: NetworkingManager
    private var localItemsModel: [ItemModel]?
    
    init(networkingManager: NetworkingManager) {
        self.networkingManager = networkingManager
    }
    
    func attachView(view: CollectionViewControllerProtocol) {
        self.view = view
    }
    
    func dettachView() {
        view = nil
    }
    
}

extension CollectionViewPresenter {
    func getData() {
        networkingManager.downloadItems() {[weak self] result in
            switch result {
            case let .success(response):
                self?.localItemsModel = response.data
                self?.setData(model: response)
            case .failure(_):
                return
            }
        }
    }
    
    func selectedItem(index: Int) {
        if let localItemsModel = localItemsModel, localItemsModel.count > index {
            view?.selectedItem(item: localItemsModel[index])
        }
    }
}

extension CollectionViewPresenter {
    private func setData(model: ItemsModel) {
        var cells = [CollectionCellConfigurator]()
        
        guard let data = model.data else {
            return
        }
        
        data.forEach {
            if let attributes = $0.attributes {
                var cellModel = CollectionViewCell.ViewModel()
                cellModel.title = attributes.name ?? ""
                cellModel.backgroundColor = attributes.color?.create ?? .black
                
                let cellConfigurator = CustomCollectionViewCellConfigurator(cellModel: cellModel)
                cells.append(cellConfigurator)
            }
        }
        
        var viewModel = CollectionViewController.ViewModel()
        viewModel.itemModels = cells
        view?.updateViewModel(with: viewModel)
    }
    
}
