//
//  DetailsViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

private struct Configuration {
    let textViewPlaceholder = "Loading..."
}

protocol DetailsViewControllerProtocol: AnyObject {
    func updateViewModel(with model: DetailsViewController.ViewModel)
}

extension DetailsViewController {
    struct ViewModel {
        var descriptionText = ""
    }
    
    struct InititalModel {
        var itemModel: ItemModel?
    }
}

class DetailsViewController: BaseViewController {
    
    private let config = Configuration()
    private var itemModel: ItemModel?
    @UsesAutoLayout private var textView = UITextView()
    
    private let presenter = DetailsViewPresenter(networkingManager: NetworkingManager.shared)

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        if let itemModel = itemModel, let id = itemModel.id {
            presenter.getDataForItem(id)
        }
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationTitle()
    }

    deinit {
        presenter.dettachView()
    }
    
}

extension DetailsViewController {
    func setupInitialModel(model: InititalModel) {
        self.itemModel = model.itemModel
    }
}

extension DetailsViewController {
    private func setupView() {
        setupBackgroundColor()
        setTextViewConstraints()
    }

    private func setupBackgroundColor() {
        view.backgroundColor = itemModel?.attributes?.color?.create
    }

    private func setupNavigationTitle() {
        tabBarController?.navigationItem.title = setTitleStyle(itemModel?.attributes?.name ?? "")
    }

    private func setTextViewConstraints() {
        view.addSubview(textView)
        textView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        textView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        textView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        textView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        textView.backgroundColor = .clear
        textView.text = config.textViewPlaceholder
    }
    
    private func setTitleStyle(_ title: String) -> String {
        return title.enumerated().map { $0.offset % 2 == 0 ? String($0.element).uppercased() : String($0.element) }.joined()
    }
    
}

extension DetailsViewController: DetailsViewControllerProtocol {
    func updateViewModel(with model: DetailsViewController.ViewModel) {
        textView.text = model.descriptionText
    }

}
