//
//  DetailsViewPresenter.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 18.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import Foundation

class DetailsViewPresenter {
    weak var view: DetailsViewControllerProtocol?
    private let networkingManager: NetworkingManager
    
    init(networkingManager: NetworkingManager) {
        self.networkingManager = networkingManager
    }
    
    func attachView(view: DetailsViewControllerProtocol) {
        self.view = view
    }
    
    func dettachView() {
        view = nil
    }
    
}

extension DetailsViewPresenter {
    func getDataForItem(_ item: String) {
        networkingManager.downloadItemWithID(item) {[weak self] result in

            switch result {
            case let .success(response):
                self?.setData(model: response)
            case .failure(_):
                return
            }
        }
    }
    
}

extension DetailsViewPresenter {
    private func setData(model: ItemDetailsModel) {
        var viewModel = DetailsViewController.ViewModel()
        viewModel.descriptionText = model.data?.attributes?.desc ?? ""
        view?.updateViewModel(with: viewModel)
    }
    
}
