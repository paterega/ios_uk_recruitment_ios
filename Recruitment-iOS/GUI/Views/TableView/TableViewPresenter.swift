//
//  TableViewPresenter.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 18.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import Foundation

class TableViewPresenter {
    weak var view: TableViewControllerProtocol?
    private let networkingManager: NetworkingManager
    private var localItemsModel: [ItemModel]?
    
    init(networkingManager: NetworkingManager) {
        self.networkingManager = networkingManager
    }
    
    func attachView(view: TableViewControllerProtocol) {
        self.view = view
    }
    
    func dettachView() {
        view = nil
    }

    func getData() {
        networkingManager.downloadItems() {[weak self] result in
            switch result {
            case let .success(response):
                self?.localItemsModel = response.data
                self?.setData(model: response)
            case .failure(_):
                return
            }
        }
    }
    
    func selectedItem(index: Int) {
        if let localItemsModel = localItemsModel, localItemsModel.count > index {
            view?.selectedItem(item: localItemsModel[index])
        }
    }
}

extension TableViewPresenter {
    private func setData(model: ItemsModel) {
        var cells = [CellConfigurator]()
        
        guard let data = model.data else {
            return
        }
        
        data.forEach {
            if let attributes = $0.attributes {
                var cellModel = TableViewCell.ViewModel()
                cellModel.title = attributes.name ?? ""
                cellModel.subtitle = attributes.preview ?? ""
                cellModel.backgroundColor = attributes.color?.create ?? .black
                
                let cellConfigurator = TableViewCellConfigurator(cellModel: cellModel)
                cells.append(cellConfigurator)
            }
        }
        
        var viewModel = TableViewController.ViewModel()
        viewModel.itemModels = cells
        view?.updateViewModel(with: viewModel)
    }
    
}

