//
//  TableViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

private struct Configuration {
    let backgroundColor = UIColor.white
}

protocol TableViewControllerProtocol: AnyObject {
    func updateViewModel(with model: TableViewController.ViewModel)
    func selectedItem(item: ItemModel)
}

extension TableViewController {
    struct ViewModel {
        var itemModels = [CellConfigurator]()
    }
}

class TableViewController: BaseTableViewControler {
    private let config = Configuration()
    private let presenter = TableViewPresenter(networkingManager: NetworkingManager.shared)
    var selectedItem: ItemModelClosure?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        presenter.getData()
        setupTableView()
        setupBackgroundColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
         setEmptyViewControllerTitle()
     }
    
    deinit {
        presenter.dettachView()
    }
    
}

extension TableViewController {
    private func setupBackgroundColor() {
        view.backgroundColor = config.backgroundColor
    }
    
    private func setEmptyViewControllerTitle() {
        tabBarController?.navigationItem.title = ""
    }
    
    private func setupTableView() {
        tableView.register(TableViewCell.self)
        tableViewDelegate.selectedCell = { [weak self] index in
            self?.presenter.selectedItem(index: index)
        }
    }
    
}


extension TableViewController: TableViewControllerProtocol {
    func updateViewModel(with model: TableViewController.ViewModel) {
        addCells(model.itemModels)
    }
    
    func selectedItem(item: ItemModel) {
        selectedItem?(item)
    }
    
}
