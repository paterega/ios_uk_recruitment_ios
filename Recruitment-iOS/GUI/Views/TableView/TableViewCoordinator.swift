//
//  TableViewCoordinator.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 18.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

private struct Configuration {
    let tabBatTitle = "TableView"
}

class TableViewCoordinator: Coordinator {
    private let config = Configuration()
    var navigationController: UINavigationController
    weak var tabBarController: UITabBarController?
    
    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
        start()
    }
    
    func start() {
        showTableViewController()
    }
}

extension TableViewCoordinator {
    func showTableViewController() {
        let tableViewController = TableViewController()
        tableViewController.selectedItem = { [weak self] model in
            self?.presentDetailsViewController(model)
        }
        
        tableViewController.onFinish = {[weak self] in
            self?.tabBarController?.navigationController?.popViewController(animated: true)
        }
        
        navigationController.title = config.tabBatTitle
        navigationController.viewControllers = [tableViewController]
    }
    
    func presentDetailsViewController(_ model: ItemModel) {
        let detailsViewController = DetailsViewController()
        let initialModel = DetailsViewController.InititalModel(itemModel: model)
        detailsViewController.setupInitialModel(model: initialModel)
        self.tabBarController?.tabBar.isHidden = true
        detailsViewController.onFinish = { [weak self] in
            self?.tabBarController?.tabBar.isHidden = false
            self?.navigationController.popViewController(animated: true)
        }
        navigationController.pushViewController(detailsViewController, animated: true)
    }
    
}
