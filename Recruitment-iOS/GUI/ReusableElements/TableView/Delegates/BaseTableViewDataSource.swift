//
//  BaseTableViewDataSource.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 19.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

class BaseTableViewDataSource: NSObject, UITableViewDataSource {
    var items =  [CellConfigurator]()

    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count > 0 ? 1 : 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId) else {
             fatalError("Unable to dequeue cell")
        }
        item.configure(cell: cell)
        return cell
    }

}
