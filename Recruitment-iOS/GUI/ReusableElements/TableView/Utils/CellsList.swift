//
//  CellsList.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 19.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import Foundation

// TableView
typealias TableViewCellConfigurator = TableCellConfigurator<TableViewCell, TableViewCell.ViewModel>
// CollectionView
typealias CustomCollectionViewCellConfigurator = CollectionViewCellConfigurator<CollectionViewCell, CollectionViewCell.ViewModel>
