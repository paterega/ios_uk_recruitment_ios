//
//  TableViewConfigurator.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 19.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol CellConfigurator {
    static var reuseId: String { get }
    func configure(cell: UIView)
}

class TableCellConfigurator<CellType: ConfigurableCell, CellModel>: CellConfigurator where CellType.CellModel == CellModel, CellType: UITableViewCell {
    static var reuseId: String { return String(describing: CellType.self) }

    let cellModel: CellModel

    init(cellModel: CellModel) {
        self.cellModel = cellModel
    }

    func configure(cell: UIView) {
        if let cell = cell as? CellType {
            cell.updateCell(model: cellModel)
        }
    }

}
