//
//  ConfigurableCell.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 19.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

protocol ConfigurableCell {
    associatedtype CellModel
    func updateCell(model: CellModel)
}
