//
//  TableView+Extension.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 19.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

extension UITableView {
    func register<T: UITableViewCell>(_: T.Type, reuseIdentifier: String? = nil) {
        self.register(T.self, forCellReuseIdentifier: reuseIdentifier ?? String(describing: T.self))
    }

}
