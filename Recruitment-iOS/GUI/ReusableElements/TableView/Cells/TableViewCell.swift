//
//  TableViewCell.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 19.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

private struct Configuration {
    let titleHeight: CGFloat = 22
    let subtitleTopOffset: CGFloat = 10
}

extension TableViewCell {
    struct ViewModel {
        var title = ""
        var subtitle = ""
        var backgroundColor: UIColor = .white
    }
}

class TableViewCell: UITableViewCell {
    private let config = Configuration()
    @UsesAutoLayout private var titleLabel = UILabel()
    @UsesAutoLayout private var subtitleLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        titleLabel.text = nil
        subtitleLabel.text = nil
    }
}

extension TableViewCell {
    private func setupView() {
        setTitleLabel()
        setSubtitleLabel()
    }
    
    private func setTitleLabel() {
        contentView.addSubview(titleLabel)
        titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: config.titleHeight).isActive = true
    }
    
    private func setSubtitleLabel() {
        contentView.addSubview(subtitleLabel)
        subtitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        subtitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: config.subtitleTopOffset).isActive = true
        subtitleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        subtitleLabel.numberOfLines = 0
    }
}

extension TableViewCell: ConfigurableCell {
    func updateCell(model: ViewModel) {
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
        backgroundColor = model.backgroundColor
    }
}
