//
//  BaseViewController.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 19.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

private struct Configuration {
    let buttonText = "Back"
    let buttonColor: UIColor = UIColor(red: 0, green: 0.478431, blue: 1, alpha: 1)
    let buttonTextFont: UIFont = UIFont.systemFont(ofSize: 15)
}

class BaseViewController: UIViewController {
    private let config = Configuration()
    var onFinish: VoidClosure?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupCustomBackButton()
    }
    
}

extension BaseViewController {
    private func hideNavigationBar() {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    private func setupCustomBackButton() {
        let customBackButton = UIButton(type: .custom)
        customBackButton.imageView?.contentMode = .scaleAspectFit
        customBackButton.setImage(UIImage(named: "backButton"), for: .normal)
        customBackButton.setAttributedTitle(setAttibutedTitleText(), for: .normal)
        customBackButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: -8, bottom: 10, right: 0)
        customBackButton.frame = CGRect(x: 0, y: 0, width: 60, height: 30)
        customBackButton.addTarget(self, action: #selector(self.backAction(sender:)), for: .touchUpInside)
        let barItemVackButton = UIBarButtonItem(customView: customBackButton)
        tabBarController?.navigationItem.leftBarButtonItem = barItemVackButton
    }
    
    private func setAttibutedTitleText() -> NSAttributedString {
        let string = config.buttonText
        let myAttributes = [NSAttributedString.Key.foregroundColor: config.buttonColor,
                            NSAttributedString.Key.font: config.buttonTextFont]
        return NSAttributedString(string: string, attributes: myAttributes)
    }
    
    @objc private func backAction(sender: UIBarButtonItem) {
        onFinish?()
    }

}
