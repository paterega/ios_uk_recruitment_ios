//
//  TabBarController.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 18.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    var tableViewCoordinator = TableViewCoordinator()
    var collectionViewCoordinator = CollectionViewCoordinator()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setupViewControllers()
    }
    
}

extension TabBarController {
    private func setupViewControllers() {
        self.delegate = self
        viewControllers = [tableViewCoordinator.navigationController, collectionViewCoordinator.navigationController]
        
        tableViewCoordinator.tabBarController = self
        collectionViewCoordinator.tabBarController = self
    }
    
}
