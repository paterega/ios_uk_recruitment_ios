//
//  BaseTableViewController.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 19.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

class BaseTableViewControler: BaseViewController {
    @UsesAutoLayout var tableView = UITableView()
    var tableViewDelegate = BaseTableViewDelegate()
    var tableViewDataSource = BaseTableViewDataSource()
    
    private(set) var cells = [CellConfigurator]() {
        didSet {
            tableViewDataSource.items = cells
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func addCells(_ cells: [CellConfigurator]) {
        self.cells = cells
    }
}

extension BaseTableViewControler {
    private func setupView() {
        tableView.delegate = tableViewDelegate
        tableView.dataSource = tableViewDataSource
        tableViewDelegate.source = tableViewDataSource
        
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.bounces = false
        tableView.separatorStyle = .none
        
        setTableViewConstraints()
    }
    
    private func setTableViewConstraints() {
        view.addSubview(tableView)
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
}
