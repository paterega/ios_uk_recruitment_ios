//
//  BaseCollectionViewController.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 21.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

class BaseCollectionViewController: BaseViewController {
    var collectionView: UICollectionView!
    var collectionViewDelegate = BaseCollectionViewDelegate()
    var collectionViewDataSource = BaseCollectionViewDataSource()
    
    private(set) var cells = [CollectionCellConfigurator]() {
        didSet {
            collectionViewDataSource.items = cells
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    func addCells(_ cells: [CollectionCellConfigurator]) {
        self.cells = cells
    }
    
}

extension BaseCollectionViewController {
    private func setupView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        collectionView.collectionViewLayout = layout
        collectionView.decelerationRate = UIScrollView.DecelerationRate.fast
        collectionView.backgroundColor = .white
        
        collectionView.dataSource = collectionViewDataSource
        collectionView.delegate = collectionViewDelegate
        
        setCollectionViewConstraints()
    }
    
    private func setCollectionViewConstraints() {
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
}
