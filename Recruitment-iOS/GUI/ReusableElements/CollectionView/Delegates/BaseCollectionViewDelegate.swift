//
//  BaseCollectionViewDelegate.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 21.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

private struct Configuration {
    let padding: CGFloat = 25
}

class BaseCollectionViewDelegate: NSObject, UICollectionViewDelegateFlowLayout {
    private let config = Configuration()
    var selectedCollectionViewCell: IntClosure?

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionCellSize = collectionView.frame.size.width - config.padding
        return CGSize(width: collectionCellSize/2, height: collectionCellSize/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCollectionViewCell?(indexPath.row)
    }
}
