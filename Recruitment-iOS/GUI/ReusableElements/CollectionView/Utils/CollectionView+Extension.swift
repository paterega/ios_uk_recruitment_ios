//
//  CollectionView+Extension.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 21.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

extension UICollectionView {
    func register<T: UICollectionViewCell>(_: T.Type, reuseIdentifier: String? = nil) {
        self.register(T.self, forCellWithReuseIdentifier: reuseIdentifier ?? String(describing: T.self))
    }

}
