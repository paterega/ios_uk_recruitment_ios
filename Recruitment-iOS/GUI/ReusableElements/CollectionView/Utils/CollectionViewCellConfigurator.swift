//
//  CollectionViewCellConfigurator.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 21.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol CollectionCellConfigurator {
    static var reuseId: String { get }
    func configure(cell: UIView)
}

class CollectionViewCellConfigurator<CellType: ConfigurableCell, CellModel>: CollectionCellConfigurator where CellType.CellModel == CellModel, CellType: UICollectionViewCell {

    static var reuseId: String { return String(describing: CellType.self) }
    let cellModel: CellModel

    init(cellModel: CellModel) {
        self.cellModel = cellModel
    }

    func configure(cell: UIView) {
        if let cell = cell as? CellType {
            cell.updateCell(model: cellModel)
        }
    }

}
