//
//  CollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 21.03.2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

extension CollectionViewCell {
    struct ViewModel {
        var title = ""
        var backgroundColor: UIColor = .cyan
    }
}

class CollectionViewCell: UICollectionViewCell {
    @UsesAutoLayout private var titleLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        titleLabel.text = nil
    }
}

extension CollectionViewCell {
    private func setupView() {
        setTitleLabel()
    }

    private func setTitleLabel() {
        contentView.addSubview(titleLabel)
        titleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        titleLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 22).isActive = true
        titleLabel.numberOfLines = 1
        titleLabel.textAlignment = .center
    }

}

extension CollectionViewCell: ConfigurableCell {
    func updateCell(model: ViewModel) {
        titleLabel.text = model.title
        contentView.backgroundColor = model.backgroundColor
    }
}
