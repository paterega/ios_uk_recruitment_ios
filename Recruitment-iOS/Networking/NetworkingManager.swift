//
//  NetworkingManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class NetworkingManager: NSObject {
    
    static let shared = NetworkingManager()
    
    enum ParseError: Error {
        case parsingError
    }
    
    func downloadItems(_ completionHandler: @escaping ((Result<ItemsModel,Error>)-> ())) {
        request(filename: "Items.json", completionBlock: {(result: Result<ItemsModel, NetworkingManager.ParseError>) in
            switch result {
            case let .success(response):
                completionHandler(.success(response))
            case .failure(_):
                completionHandler(.failure(ParseError.parsingError))
            }
        })
    }
    
    func downloadItemWithID(_ id: String, completionHandler: @escaping  ((Result<ItemDetailsModel,Error>)-> ())) {
        let filename = "Item\(id).json"
        request(filename: filename, completionBlock: {(result: Result<ItemDetailsModel, NetworkingManager.ParseError>) in
            switch result {
            case let .success(response):
                completionHandler(.success(response))
            case .failure(_):
                completionHandler(.failure(ParseError.parsingError))
            }
        })
    }
    
}

extension NetworkingManager {
    private func request<T:Codable>(filename:String, completionBlock:@escaping ((Result<T,ParseError>) -> ())) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let object = JSONParser.jsonFromFilename(T.self, filename: filename) {
                completionBlock(.success(object))
            } else {
                completionBlock(.failure(.parsingError))
            }
        }
    }
}
