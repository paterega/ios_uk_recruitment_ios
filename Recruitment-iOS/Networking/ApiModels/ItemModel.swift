//
//  ItemModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

struct ItemsModel: Codable {
    var data: [ItemModel]?
}

struct ItemModel: Codable {
    var id: String?
    var attributes: ItemModelAttributes?
}

struct ItemModelAttributes: Codable {
    var name: String?
    var preview: String?
    var color: Color?
}

enum Color: String, Codable {
    case red = "Red"
    case blue = "Blue"
    case green = "Green"
    case yellow = "Yellow"
    case purple = "Purple"
    case black = ""
    
    var create: UIColor {
        switch self {
        case .red:
            return UIColor.red
        case .blue:
            return UIColor.blue
        case .green:
            return UIColor.green
        case .yellow:
            return UIColor.yellow
        case .purple:
            return UIColor.purple
        case .black:
            return UIColor.black
        }
    }
}

