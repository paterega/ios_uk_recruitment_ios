//
//  ItemDetailsModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

struct ItemDetailsModel: Codable {
    var data: ItemDetailModel?
}

struct ItemDetailModel: Codable {
    var id: String?
    var attributes: ItemDetailsModelAttributes?
}

struct ItemDetailsModelAttributes: Codable {
    var name: String?
    var desc: String?
    var color: Color?
}
